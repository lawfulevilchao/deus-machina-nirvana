#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Built on top of basic.py: https://github.com/wikimedia/pywikibot/blob/master/scripts/basic.py

Partial matches on names may be necessary (e.g. "Terumi" "Mei" "Kuon")
Only adds Gallery Metadata to Files that are in at least one Gallery Section
(Files categorized in a subcategory of "Category:Media by Gallery Section")

python pwb.py scripts/userscripts/process_gallerymetadata.py -simulate -cat:"BlazBlue: Alter Memory" -summary:"Update" -debug2 -sort-cameos -default-to-character -limit:"10"

---

&params;

The following parameters are supported:

All standard pywikibot parameters, including:

-always           The bot won't ask for confirmation when putting a page
-cat:"category name here"
                  Run the script on all images in the specified category.
                  The bot skips images that already have Template:Gallery Metadata.
-catfilter:""
                  Filter images in <cat> further by this category
-simulate         No changes to live wiki will be made.
-summary          Set the action summary message for the edit.

And these paramaters:

-addOnly
    skip pages with existing Template:Gallery Metadata
-debug2           Print verbose debug text specific to this bot
-default-to-character
    If none of the tagged characters are in the image's title, mark them all as characters.
    e.g. for generic Story images that are not specific to a certain character
    This overrides sort-cameos if no characters are matched in the title.
-limit:"2"        Limit the number of images to be worked on
-sort-cameos
    Tagged characters whose names don't appear in the image's title are marked as cameos rather than characters.
    e.g. for Arcade Mode images or Birthday Art
"""
#
# (C) Pywikibot team, 2006-2019
#
# Distributed under the terms of the MIT license.
#
from __future__ import absolute_import, division, unicode_literals

import os
import pickle
import re

from operator import methodcaller

import pywikibot

from pywikibot import config,pagegenerators
from pywikibot.bot import (
    SingleSiteBot, ExistingPageBot, NoRedirectPageBot, AutomaticTWSummaryBot
)
from pywikibot.tools import (
    open_archive
)

# This is required for the text that is shown when you run this script
# with the parameter -help.
docuReplacements = {'&params;': pagegenerators.parameterHelp}  # noqa: N816

# https://stackoverflow.com/a/21786287
color_head = '\x1b[1;31;40m'
color_em = '\x1b[0;33;40m'
color_tail = '\x1b[0m'

DEBUG = False

def debug_output(*args):
    if DEBUG:
        pywikibot.output(*args)

class GalleryMetadataTemplateSettings():
    def __init__(self, cat_db):
        self.cat_db = cat_db

        # boxes as defined as supercategories on the wiki
        self.sources = ["Category:Media by Source","Category:Media from Manga","Category:Media from Novels","Category:Media from Collaborations"]
        self.sections = ["Category:Media by Gallery Category"]
        self.artists = ["Category:Media by Artist"]
        self.characters = ["Category:Media by Character"]
        self.locations = ["Category:Media by Location"]
        self.weapons = ["Category:Media by Weapon"]

        # holds all subcategories in self.sources
        self._valid_sources = []
        self.__set_valid_sources()

        # ambiguous templates to add to the top of the page if an ambiguity is found
        self.ambigNotice = "{{Contains Characters With Shared Names}}\n"
        self.needsReviewNotice = "{{Needs Verification of Ambiguous Characters}}"

    def __set_valid_sources(self):
        # compile a list of valid source material from the categories in sources
        for supercat_title in self.sources:
            # sort them to assign expectable numbers
            supercat = pywikibot.Category(pywikibot.Site(),supercat_title)
            valid_source = sorted(self.cat_db.getSubcats(supercat),key=methodcaller('title'))
            for vs in valid_source:
                self._valid_sources.append(vs.title().split(":",1)[1].encode('UTF8')) # convert from unicode to string for later string comparison

# @defaultToCharacter: whether to default to Character if no characters are found in the title
# @wiki: pass in the GalleryMetadataTemplateSettings, which are really just a description of the wiki's category structure, hence this argument's name
class GalleryMetadataTemplatePageHelper():
    def __init__(self, current_page, wiki, cat_db, sortCameos, defaultToCharacter):
        self.current_page = current_page
        self.wiki = wiki
        self.cat_db = cat_db

        self.sortCameos = sortCameos
        self.defaultToCharacter = defaultToCharacter

        self.preserved_apostrophe = "¢"

        # boxes for the template
        self._sources = []
        self._sections = []
        self._artists = []
        self._characters = []
        self._cameos = []        # same as characters
        self._locations = []
        self._weapons = []
        self._other = []

        # variables for checking for ambiguous characters
        self._checked_names = []
        self.isAmbiguous = False
        self.containsAmbiguousNotice = False
        self.templatesection_characters = ""
        self.templatesection_cameos = ""

    def __set_contains_ambigNotice(self):
        # returns True if the current page has a prior existing notice for ambiguous characters
        text = self.current_page.text

        m = re.search(self.wiki.ambigNotice,text)
        if m:
            self.containsAmbiguousNotice = True

    def __preserve_characters_and_cameos(self):
        # intializes self.templatesection_characters and self.templatesection_cameos
        text = self.current_page.text

        m = re.search("{{Gallery Metadata\\|(.*?)}}",text) # matches {{Gallery Metadata|...}}
        if m:
            template = text[m.start():m.end()]

            # save the pre-existing characters/cameos section
            # matches |characters=... until either | or }
            param_chara = re.search("\\|characters=(.*?)(\\n?)[\\|]",template)
            if param_chara:
                self.templatesection_characters = template[param_chara.start():param_chara.end() - 1] # cut off the ending | or }

            param_cameo = re.search("\\|cameos=(.*?)(\\n?)[\\|]",template)
            if param_cameo:
                self.templatesection_cameos = template[param_cameo.start():param_cameo.end() - 1] # cut off the ending | or }

    def __get_page_source(self):
        # returns what's in the "source" field of the Information template
        # if it's a valid source
        
        text = self.current_page.text

        m = re.search("\\|source=(.*?)\n\\|",text) # matches |source=...\n|
        if m:
            s = m.group(0).split("|",-1) # split |source=BlazBlue:...\n| at every | sign

            #source = m.group(0)
            source = s[1]
            source = source.split("=",1)[1] # split |source=BlazBlue:...\n| at the first = sign
            source = source.split("\n")[0] # cut off the newline

            debug_output(color_head + "\nValid sources:" + color_tail + " {}".format(self.wiki._valid_sources))
            
            # check that this source is one of the children of the lists of sources
            if source in self.wiki._valid_sources:
                debug_output(color_head + "\nThe listed source " + color_tail 
                                  + "(" + source + ")" 
                                  + color_head + " is in the list of valid sources" + color_tail)
                return source
            else:
                debug_output(color_head + "\nThe listed source " + color_tail 
                                  + "(" + source + ")" 
                                  + color_head + " is not in the list of valid sources" + color_tail)

    def __get_page_caption(self):
        text = self.current_page.text

        m = re.search("{{#set:Caption=(.*?)}}",text) # matches {{#set:Caption=...}}

        if m:
            caption = m.group(0)
            caption = caption.split("=",1)[1] # split {{#set:Caption=...}} at the first = sign
            caption = caption.split("}",1)[0] # remove the trailing }}
            caption = caption.replace("'", self.preserved_apostrophe) # preserve apostrophes in the caption

            debug_output("caption: \n{}".format(caption))

            return caption
        else:
            return None

    def __is_focal_character(self, title, character):
        # check if any of the parts of the character's name match part of the title (e.g. Valkenhayn, Terumi, Es)
        isFocal = False

        # blacklist some common words
        blacklist = ["the", "an", "of"]

        # split name into parts, removing apostrophes since they don't match
        names = character.title().replace("'","").split(" ")

        # split title into parts, also removing apostrophes
        title_parts = title.replace("'","").split(" ")

        # check if any (whole) part matches.
        # This is to prevent e.g. Es from matching for Estella and Es-N
        # However, this will return true for both Hinata Himezuru and Yuki Himezuru bc of shared last name
        # so we also check if any part of the name matches parts of a name we've already checked
        # and if it does, we mark the file as ambiguous
        for name in names:

            if not name in blacklist:
                if name in self._checked_names:
                    self.isAmbiguous = True
                else:
                    self._checked_names.append(name)
            
                if name in title_parts:
                    # successful match, end early
                    debug_output(color_head + "Found part of this character's name " + color_tail
                                  + "(" + name + ")" 
                                  + color_head + " in the filename" + color_tail);
                    isFocal = True

        return isFocal

    def __categorize_category(self, category, supercats):
        title = self.current_page.title()

        category_title = u"{}".format(category.title().split(":",1)[1])
        debug_output(u'{}{}{}, in:'.format(color_em, category_title, color_tail))
        
        # try to categorize the category based on its super categories. Stop at the first one that fits.
        is_categorized = False
        for supercat in supercats:
            supercat_title = supercat.title()
            debug_output('  {}'.format(supercat_title))

            if not is_categorized: 
                if supercat_title in self.wiki.sources:
                    self._sources.append(category_title)
                    is_categorized = True

                if supercat_title in self.wiki.sections:
                    self._sections.append(category_title)
                    is_categorized = True

                if supercat_title in self.wiki.artists:
                    self._artists.append(category_title)
                    is_categorized = True

                if supercat_title in self.wiki.characters:
                    if self.__is_focal_character(title, category_title):
                        self._characters.append(category_title)
                    else:
                        # not a focal character
                        if self.sortCameos:
                            self._cameos.append(category_title)
                        else:
                            self._characters.append(category_title)
                    is_categorized = True

                if supercat_title in self.wiki.locations:
                    self._locations.append(category_title)
                    is_categorized = True

                if supercat_title in self.wiki.weapons:
                    self._weapons.append(category_title)
                    is_categorized = True
        
        # if this category is still not categorized, append it directly to _other (i.e. uncategorized)
        if not is_categorized:
            self._other.append(category_title)
            debug_output('  no parent categories, or unrecognized parent category')

    def __build_template(self):

        text = self.current_page.text

        # format Gallery Metadata template
        template = ""

        # Try to fill in the source field of the Gallery Metadata based on the existing source label
        source = self.__get_page_source()
        if source:
            if not source in self._sources: # append only if it's not a duplicate
                text = text + "\n[[Category:" + source + "]]"
                self._sources.append(source)

        # Try to fill in the caption of the Gallery Metadata based on existing {{#set:Caption=...}}
        caption = self.__get_page_caption()

        if len(self._sources) > 0:
            template += "|source={}".format(",".join(self._sources))
        if len(self._sections) > 0:
            template += "|section={}".format(",".join(self._sections))
        if len(self._artists) > 0:
            template += "|artists={}".format(",".join(self._artists))

        if self.containsAmbiguousNotice:
            # use the old characters and cameos sections, if any
            template += self.templatesection_characters
            template += self.templatesection_cameos
        else:
            if len(self._characters) > 0:
                template += "|characters={}".format(",".join(self._characters))
            if len(self._cameos) > 0:
                template += "|cameos={}".format(",".join(self._cameos))

        if len(self._locations) > 0:
            template += "|locations={}".format(",".join(self._locations))
        if len(self._weapons) > 0:
            template += "|weapons={}".format(",".join(self._weapons))
        if caption:
            template += "|caption=" + caption

        if template != "":
            template = template.replace("'","")   # strip all apostrophes because characters can't have them in their name
            template = template.replace(self.preserved_apostrophe,"\'") # put back apostrophes in the caption
            template = "{{Gallery Metadata" + template + "}}"

        debug_output('\n' + color_head + 'Template:\n' + color_tail + color_em + repr(template) + color_tail)

        return template

    def create_template(self):
        # Fill out Gallery Metadata based on the categories on the page.
        template = ""

        # initialize data
        self.__set_contains_ambigNotice()
        if self.containsAmbiguousNotice:
            self.__preserve_characters_and_cameos()

        debug_output("\n" + color_head + "Categories" + color_tail)
        
        # categorize each category on the page according to its super-category
        for category in self.current_page.categories():
            # get super-categories
            # sort them to assign expectable numbers
            supercats = sorted(self.cat_db.getSupercats(category),key=methodcaller('title'))
            self.__categorize_category(category, supercats)

        # print out uncategorized categories
        if len(self._other) > 0:
            debug_output('\nUncategorized categories: \n{}'.format(",".join(self._other)))

        # quit & return nothing if this image wasn't categorized in a Gallery Section
        # we only want to update Gallery Metadata for images that will be in galleries
        if len(self._sections) == 0:
            return template

        # if there are no characters in _characters, put all the characters from _cameos into _characters
        # i.e. for generic routes like True Ends and stuff
        if self.defaultToCharacter:
            if len(self._characters) == 0:
                debug_output(color_head + "One of the characters' names was not found in the filename; setting all characters to characters instead of cameos" + color_tail)
                self._characters.extend(self._cameos)
                del self._cameos[:]  # clear out _cameos using 2.7 syntax -- _cameos.clear() requires python 3.3+

        template = self.__build_template()

        return template

class BasicBot(
    # Refer pywikobot.bot for generic bot classes
    SingleSiteBot,  # A bot only working on one site
    # CurrentPageBot,  # Sets 'current_page'. Process it in treat_page method.
    #                  # Not needed here because we have subclasses
    ExistingPageBot,  # CurrentPageBot which only treats existing pages
    NoRedirectPageBot,  # CurrentPageBot which only treats non-redirects
    AutomaticTWSummaryBot,  # Automatically defines summary; needs summary_key
):

    """
    @ivar summary_key: Edit summary message key. The message that should be
        used is placed on /i18n subdirectory. The file containing these
        messages should have the same name as the caller script (i.e. basic.py
        in this case). Use summary_key to set a default edit summary message.

    @type summary_key: str
    """

    summary_key = 'basic-changing'

    def __init__(self, generator, cat_db, **kwargs):
        """
        Initializer.

        @param generator: the page generator that determines on which pages
            to work
        @type generator: generator

        @param cat_db: a CategoryDatabase object.
        @type: CategoryDatabase object
        """
        self.num_pages = 0

        self.cat_db = cat_db
        self.availableOptions.update({
                                # -always option is predefined by BaseBot class
            'summary': None,    # your own bot summary
            'limit': 10,        # limit '10' is default
            'debug2': False,    # verbose flag, default False
            'addOnly': False,
            'default-to-character': False,
            'sort-cameos': False,
            'rebuild-categories': False # rebuilt category database
        })

        # call initializer of the super class
        super(BasicBot, self).__init__(site=True, **kwargs)

        # assign the generator to the bot
        self.generator = generator

        # get settings
        self.settings = GalleryMetadataTemplateSettings(self.cat_db)

    def process_gallerymetadata(self, current_page):
        
        # count how many pages we've treated. If we've treated more pages than the allowed limit, quit.
        # error: comparing int and string
        #debug_output("PAGE #%s" % self.num_pages);
        #if self.num_pages >= self.opt['limit']:
        #    return
        #else:
        self.num_pages = self.num_pages + 1

        # create the template
        pageHelper = GalleryMetadataTemplatePageHelper(current_page
            , self.settings
            , self.cat_db
            , self.opt['sort-cameos']
            , self.opt['default-to-character']
        )
        template = pageHelper.create_template()

        ### do stuff to the page ###

        # get page's text and make a copy to preserve the old text
        text = self.current_page.text
        oldtext = text

        # if page already has a Gallery Metadata template in it
        # remove it if we're updating the template
        m = re.search("{{Gallery Metadata\\|(.*?)}}",text) # matches {{Gallery Metadata|...}}
        if m:
            debug_output("This page already includes the template: {}".format(m.group(0)))
            #debug_output("regex: {}".format(m.group(0)) + ", end of match: {}".format(m.end()))

            if self.opt['addOnly']:
                debug_output("Skipping this file.")
                return;
            else:
                text = text[:m.start()] + text[m.end():] # remove the existing template

        # Append the template after {{en|...}} in the description section of Template:Information on the page
        m = re.search("{{en\\|(.*?)}}",text) # matches {{en|...}}
        if m:
            #debug_output("regex: {}".format(m.group(0)) + ", end of match: {}".format(m.end()))
            text = text[:m.end()] + template + text[m.end():]
        else:
            # missing INFORMATION template, so we add it to the top of the page.

            info_template = ""
            info_template += "=={{int:filedesc}}==" 
            info_template += "\n{{Information\n|description={{en|1=}}" 
            info_template += template 
            info_template += "\n|date=\n|source={}\n|author=Arc System Works\n|permission=\n|other versions=\n}}}}".format(",".join(pageHelper._sources)) 
            info_template += "\n\n=={{int:license-header}}==\n"

            # If there isn't {{Fair Use}} on the page, add it
            m = re.search("{{Fair( )?(_)?Use}}",text) # matches {{Fair_Use}} and {{Fair Use}}
            if not m:
                info_template += "{{Fair Use}}"

            text = info_template + text

        debug_output(color_head + "\nFinal text:\n" + color_tail + "{}".format(text))

        # if summary option is None, it takes the default i18n summary from
        # i18n subdirectory with summary_key as summary key.
        self.put_current(text, summary=self.opt['summary'])

    def treat_page(self):
        self.process_gallerymetadata(self.current_page)

class CategoryDatabase(object):

    """Temporary database saving pages and subcategories for each category.
    This prevents loading the category pages over and over again.

    Borrowed from pywikibot/scripts/category.py
    """

    def __init__(self, rebuild=False, filename='category.dump.bz2'):
        """Initializer."""
        if not os.path.isabs(filename):
            filename = config.datafilepath(filename)
        self.filename = filename
        if rebuild:
            self.rebuild()

    @property
    def is_loaded(self):
        """Return whether the contents have been loaded."""
        return hasattr(self, 'catContentDB') and hasattr(self, 'superclassDB')

    def _load(self):
        if not self.is_loaded:
            try:
                if config.verbose_output:
                    pywikibot.output('Reading dump from '
                                     + config.shortpath(self.filename))
                with open_archive(self.filename, 'rb') as f:
                    databases = pickle.load(f)
                # keys are categories, values are 2-tuples with lists as
                # entries.
                self.catContentDB = databases['catContentDB']
                # like the above, but for supercategories
                self.superclassDB = databases['superclassDB']
                del databases
            except Exception:
                # If something goes wrong, just rebuild the database
                self.rebuild()

    def rebuild(self):
        """Rebuild the dabatase."""
        self.catContentDB = {}
        self.superclassDB = {}

    def getSubcats(self, supercat):
        """Return the list of subcategories for a given supercategory.
        Saves this list in a temporary database so that it won't be loaded
        from the server next time it's required.
        """
        self._load()
        # if we already know which subcategories exist here
        if supercat in self.catContentDB:
            return self.catContentDB[supercat][0]
        else:
            subcatset = set(supercat.subcategories())
            articleset = set(supercat.articles())
            # add to dictionary
            self.catContentDB[supercat] = (subcatset, articleset)
            return subcatset

    def getArticles(self, cat):
        """Return the list of pages for a given category.
        Saves this list in a temporary database so that it won't be loaded
        from the server next time it's required.
        """
        self._load()
        # if we already know which articles exist here.
        if cat in self.catContentDB:
            return self.catContentDB[cat][1]
        else:
            subcatset = set(cat.subcategories())
            articleset = set(cat.articles())
            # add to dictionary
            self.catContentDB[cat] = (subcatset, articleset)
            return articleset

    def getSupercats(self, subcat):
        """Return the supercategory (or a set of) for a given subcategory."""
        self._load()
        # if we already know which subcategories exist here.
        if subcat in self.superclassDB:
            return self.superclassDB[subcat]
        else:
            supercatset = set(subcat.categories())
            # add to dictionary
            self.superclassDB[subcat] = supercatset
            return supercatset

    def dump(self, filename=None):
        """Save the dictionaries to disk if not empty.
        Pickle the contents of the dictionaries superclassDB and catContentDB
        if at least one is not empty. If both are empty, removes the file from
        the disk.
        If the filename is None, it'll use the filename determined in __init__.
        """
        if filename is None:
            filename = self.filename
        elif not os.path.isabs(filename):
            filename = config.datafilepath(filename)
        if self.is_loaded and (self.catContentDB or self.superclassDB):
            pywikibot.output('Dumping to {}, please wait...'
                             .format(config.shortpath(filename)))
            databases = {
                'catContentDB': self.catContentDB,
                'superclassDB': self.superclassDB
            }
            # store dump to disk in binary format
            with open_archive(filename, 'wb') as f:
                try:
                    pickle.dump(databases, f, protocol=config.pickle_protocol)
                except pickle.PicklingError:
                    pass
        else:
            try:
                os.remove(filename)
            except EnvironmentError:
                pass
            else:
                pywikibot.output('Database is empty. {} removed'
                .format(config.shortpath(filename)))

def main(*args):
    """
    Process command line arguments and invoke bot.

    If args is an empty list, sys.argv is used.

    @param args: command line arguments
    @type args: str
    """
    rebuild = False # rebuild CategoryDatabase?
    options = {}

    # Process global arguments and prepare generator args parser
    local_args = pywikibot.handle_args(args)
    gen_factory = pagegenerators.GeneratorFactory()

    # Parse command line arguments
    for arg in local_args:

        # Catch the pagegenerators options
        if gen_factory.handleArg(arg):
            continue  # nothing to do here

        # Now pick up your own options
        arg, sep, value = arg.partition(':')
        option = arg[1:]
        if option in ('summary', 'limit', 'debug2', 'default-to-character', 'sort-cameos', 'addOnly', 'rebuild-categories'):
            if not value:
                # pywikibot.input('Please enter a value for ' + arg)
                options[option] = True
            else:
                options[option] = value
            pywikibot.output("[param] %s: %s" % (option, options[option]));
        # take the remaining options as booleans.
        # You will get a hint if they aren't pre-defined in your bot class
        else:
            options[option] = True

    if 'debug2' in options:
        pywikibot.output("[param] %s: %s" % ('debug2', options['debug2']));
        DEBUG = True
    if 'rebuild-categories' in options:
        rebuild = True

    cat_db = CategoryDatabase(rebuild=rebuild)

    # The preloading option is responsible for downloading multiple
    # pages from the wiki simultaneously.
    gen = gen_factory.getCombinedGenerator(preload=True)
    if gen:
        bot = BasicBot(gen, cat_db, **options)
        try:
            bot.run()
        except pywikibot.Error:
            pywikibot.error('Fatal error:', exc_info=True)
        finally:
            if cat_db:
                cat_db.dump()
            return True
    else:
        pywikibot.bot.suggest_help(missing_generator=True)
        return False


if __name__ == '__main__':
    main()
