#!/usr/bin/python
# -*- coding: utf-8 -*-
"""
Built on top of basic.py: https://github.com/wikimedia/pywikibot/blob/master/scripts/basic.py

@param hours: How many hours back to process files for. Defaults to 1
@type hours: int?

This script takes recently updated files and calls process_gallerymetadata.py on each
- only processes Files
- includes duplicates
- includes edits, new pages, and log entries
- skips bot edits
"""
#
# (C) Pywikibot team, 2006-2019
#
# Distributed under the terms of the MIT license.
#
from __future__ import absolute_import, division, unicode_literals

import sys

import pywikibot
from pywikibot import pagegenerators

from datetime import timedelta
import subprocess

def main(*args):

    FREQUENCY = 1 # default 1, takes value of 'hours' arg later if available
    NS_FILE = 6 # File namespace, https://www.mediawiki.org/wiki/Manual:Namespace

    #Grab the locations...
    _python_exec = sys.executable or "python"
    _pwb_base = pywikibot.argvu[0][:-36] # cut off "scripts/userscripts/process_files.py" (36characters)

    # Process global arguments
    local_args = pywikibot.handle_args(args)

    pywikibot.output("*** PYWIKIBOT.process_files ***")
    pywikibot.output("python executable location: %s" % sys.executable)
    pywikibot.output("pwb base location: %s" % _pwb_base)

    # Parse command line argument 'hours'
    for arg in local_args:
        arg, sep, value = arg.partition(':')
        option = arg[1:]
        if option == 'hours':
            if value:
                FREQUENCY = int(value)
            pywikibot.output("[param] %s: %s" % (option, FREQUENCY));
        # discard all other arguments

    site = pywikibot.Site()

    timeOfCurrentRun = site.server_time()
    timeOfLastRun = timeOfCurrentRun - timedelta(hours=FREQUENCY)

    pywikibot.output("Time of Last Run: %s" % timeOfLastRun)
    pywikibot.output("Time of Current Run: %s" % timeOfCurrentRun)

    ### process recent changes between timeOfLastRun and timeOfCurrentRun ###

    # https://doc.wikimedia.org/pywikibot/master/_modules/pywikibot/pagegenerators.html#RecentChangesPageGenerator
    gen = pagegenerators.RecentChangesPageGenerator(start=timeOfLastRun
        , end=timeOfCurrentRun
        , reverse=True
        , namespaces=[NS_FILE]
        , bot=False
    )

    pages_worked_on = 0
    for page in gen:
        pages_worked_on = pages_worked_on + 1
        pagename = page.title()

        # make sure the filename is in File:X format
        pywikibot.output("[%03d] %s" % (pages_worked_on, pagename))

        command = [
              _python_exec
            , _pwb_base + "pwb.py"
            , _pwb_base + "scripts/userscripts/process_gallerymetadata.py"
            , "-page:" + pagename
            , "-sort-cameos"
            , "-default-to-character"
            , '-summary:"Update Gallery Metadata"'
            , "-always"
            , "-addOnly"
            #, "-log"
        ]
       
        # refresh categories once per call to process_files.py
        if pages_worked_on == 1:
            command.append("-rebuild-categories")

        output = subprocess.check_output(command)
        #pywikibot.output(output)

    pywikibot.output("Pages worked on: %d" % pages_worked_on)

if __name__ == '__main__':
    main()