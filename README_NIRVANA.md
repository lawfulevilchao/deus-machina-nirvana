README

#Overview

[Blazblue.wiki](https://blazblue.wiki) has several thousands of images neatly named and categorized. With Semantic MediaWiki, these categories could be leveraged to [dynamically fill galleries with relevant images](https://blazblue.wiki/wiki/Help:Manual_of_Style/Character_Pages/Gallery).

Recently, the wiki wished to transition to using [Cargo](https://www.mediawiki.org/wiki/Extension:Cargo) to store the data for creating galleries, because it provides more flexibility in how data is treated. (Specifically, it allows us to split appearing characters into "main" and "sub" roles - referenced as "character" and "cameo," respectively, in the system). However, transitioning completely to using Cargo is undesirable, since removing categories in favor of Cargo would severely increase the barrier to entry for editing and produce bugs that are hard to track and laborious to fix: filling data into templates manually is typo-prone and redundant. It also marginalizes categories, which are more accessible, visible, and easier to understand than Cargo tables.

The solution was to have a script maintain the templates for Cargo, since all the data needed to be in Cargo is already contained within the category hierarchy the wiki uses.

#How this works

At set intervals, a cron job (is intended to) run *process_files.py*. *process_files.py* goes through all recently-uploaded and edited images that have been touched since the last specified time (given in hours before the current time), and calls *process_gallerymetadata.py* to process or re-process each page's Gallery Metadata as needed.

*process_gallerymetadata.py* adds Template:Gallery Metadata to images.

See the individual files for details on their arguments.

#Setup

1. To install pywikibot, see https://www.mediawiki.org/wiki/Manual:Pywikibot/Installation
2. Assuming a fresh pywikibot installation, unzip the contents into the top level (`core_stable` or similar) of the pywikibot installation.
3. Try `python pwb.py login` to check if the installation is working. Login with the password to User:Deus Machina Nirvana.